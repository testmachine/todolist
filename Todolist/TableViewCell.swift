//
//  TableViewCell.swift
//  Todolist
//
//  Created by MacBookPro on 26/08/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var buttonCheck: UIButton!
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelSubItems: UILabel!
    
    @IBAction func buttonCheckAction(_ sender: Any) {
        toDoInCell?.changeState()
        setCheckButton()
        saveData()
    }
    
    var toDoInCell: ToDoItem?
    
    func initCell(toDo: ToDoItem) {
        toDoInCell = toDo
        labelName.text = toDoInCell?.name
        labelSubItems.text = toDoInCell?.subItemsText
    }
    
    func setCheckButton() {
        if toDoInCell!.isComplete {
            buttonCheck.setImage(#imageLiteral(resourceName: "checked.png"), for: .normal)
        } else {
            buttonCheck.setImage(#imageLiteral(resourceName: "unchecked.png"), for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

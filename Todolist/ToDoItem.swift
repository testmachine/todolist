//
//  ToDoItem.swift
//  Todolist
//
//  Created by MacBookPro on 17/08/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit

class ToDoItem: NSObject, NSCoding {
    var name: String = ""
    var isComplete: Bool = false
    
    var subItems: [ToDoItem] = []
    
    var subItemsText: String {
        if subItems.count == 0 {
            return ""
        }
        var completedToDoCount: Int = 0
        for todo in subItems {
            if todo.isComplete {
                completedToDoCount += 1
            }
        }
        
        if completedToDoCount == 0 {
            return "\(subItems.count) subitems"
        } 
        return "\(subItems.count) subitems / \(completedToDoCount) completed"
    }
    
    var uncompletedSubitems: Int {
        var unCompletedToDoCount: Int = 0
        for todo in subItems {
            if todo.isComplete {
                unCompletedToDoCount += 1
            }
        }
        return unCompletedToDoCount
    }
    
    var dictionary: NSDictionary? {
        var arraySubToDos = NSArray()
        
        for item in subItems {
            arraySubToDos = arraySubToDos.adding(item) as NSArray
        }
        
        let dictionary = NSDictionary(objects: [name, isComplete, arraySubToDos], forKeys: ["name" as NSString, "isComplete" as NSString, "subtodos" as NSString])
        
        return dictionary
        
    }
    override init() {}
    
    required init(coder decoder: NSCoder) {
        if let dictionary = decoder.decodeObject(forKey: "dict") as? NSDictionary {
            self.name = dictionary.object(forKey: "name") as! String
            self.isComplete = dictionary.object(forKey: "isComplete") as! Bool
            self.subItems = []
            
            let arraySubToDos = dictionary.object(forKey: "subtodos") as! [ToDoItem]
            for subToDoItem in arraySubToDos {
                self.subItems.append(subToDoItem)
            }
        }
    }
    
    init(dictionary: NSDictionary) {
        self.name = dictionary.object(forKey: "name") as! String
        self.isComplete = dictionary.object(forKey: "isComplete") as! Bool
        self.subItems = []
        
        let arraySubToDos = dictionary.object(forKey: "subtodos") as! [ToDoItem]
        for subToDoItem in arraySubToDos {
            self.subItems.append(subToDoItem)
        }
        
    }
    
    init(name: String) {
        self.name = name
        self.isComplete = false
        self.subItems = []
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(dictionary, forKey: "dict")
    }
    
    func addSubItem(item: ToDoItem) {
        subItems.append(item)
    }
    
    func removeSubItem(index: Int) {
        subItems.remove(at: index)
    }
    
    func changeState() {
        self.isComplete = !self.isComplete
    }

}

//
//  Model.swift
//  Todolist
//
//  Created by MacBookPro on 17/08/2017.
//  Copyright © 2017 Maxim Kuznetsov. All rights reserved.
//

import UIKit

var filePath: String {
    //1 - manager lets you examine contents of a files and folders in your app; creates a directory to where we are saving it
    let manager = FileManager.default
    //2 - this returns an array of urls from our documentDirectory and we take the first path
    let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
    print("this is the url path in the documentDirectory \(url)")
    //3 - creates a new path component and creates a new file called "Data" which is where we will store our Data array.
    return (url!.appendingPathComponent("Data").path)
}

var rootItem: ToDoItem?

func loadData() {
    if let dictionary = NSKeyedUnarchiver.unarchiveObject(withFile: filePath) as? NSDictionary {
        rootItem = ToDoItem(dictionary: dictionary)
    } else {
        rootItem = ToDoItem(name: "ToDo")
    }
}

func saveData() {
    if let rootItem = rootItem {
        NSKeyedArchiver.archiveRootObject(rootItem.dictionary ?? NSDictionary(), toFile: filePath)
        UIApplication.shared.applicationIconBadgeNumber = rootItem.uncompletedSubitems
    }
    
}
